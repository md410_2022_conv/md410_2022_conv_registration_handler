""" Build MDC2022 registration record markdown and PDF files from a list of supplied Registration models for a specified group
"""


import attr
from collections import defaultdict
import json
import os, os.path
from pathlib import Path

import reg_entry_file_utils

import click
from md410_2022_conv_common import models, constants, build_pdf
from rich import print


DESCRIPTIONS = {
    "reg": "Registration",
    "pins": "Convention Pin",
    "transport": "Return Airport Transfer",
    "windbreaker": "Windbreaker",
}


@attr.s
class RegistrationRenderer(object):
    registrations = attr.ib()
    group_name = attr.ib()
    out_dir = attr.ib(default=None)

    def __payment_details(self):
        self.out.append(f"")
        self.out.append(f"# Payment Details {{-}}")
        self.out.append(
            f"""\

Please make all payments to this account:

* **Bank**: FNB
* **Branch Code**: 250655
* **Account Number**: 62847830529
* **Account Name**: Port Rex Lions Club Admin account

Please make EFT payments rather than cash deposits wherever possible.

Use the reference "*{self.reg_num_text}*" when making payment. 

Please send proof of payment to [emonticonvention2022@gmail.com](mailto:emonticonvention2022@gmail.com).

## Cancellations {{-}}

* If your registration is cancelled earlier than 21 days before your date of arrival, your payment will be refunded in full except for a R50 admin fee.
* Cancellations later than 21 days before your date of arrival will not be refunded as the full expenses will already have been incurred for the registration.

Thank you again for registering for the 2022 MD410 Convention.
"""
        )

    def __attrs_post_init__(self):
        text = "/".join(f"{r.reg_num:03}" for r in self.registrations)
        self.reg_num_text = f"MD{text}"

        self.out = []
        self.out.append(f"# Registration Records for {self.group_name} {{-}}")
        self.out.append("")
        self.out.append(f"## Attendees {{-}}")
        self.out.append("")
        for r in self.registrations:
            self.append(f"* {r.attendees[0].full_name}")
        self.out.append("")
        self.out.append("## Registration Details {-}")
        self.out.append("")
        self.render_items()
        self.out.append("")
        self.out.append(
            f"# Total Cost: R{sum(r.cost for r in self.registrations)} {{-}}"
        )
        self.out.append("")
        self.out.append("")
        self.__payment_details()
        self.fn = (
            f"mdc2022_registration_{self.group_name.lower().replace(' ', '_')}.txt"
        )
        if self.out_dir:
            self.fn = os.path.join(self.out_dir, self.fn)
        self.save()

    def append(self, msg):
        self.out.append(msg.encode("ascii", "ignore").decode("ascii"))

    def render_items(self):
        items = defaultdict(int)
        for r in self.registrations:
            for (item, number) in r.items.dict().items():
                if (item != "windbreakers") and number:
                    items[item] += number

                    # cost = getattr(constants, f"COST_{item.upper()}", 0) * number
            if r.items.windbreakers:
                items["windbreaker"] += len(r.items.windbreakers)
        for (item, number) in items.items():
            self.append(
                f"* **{number} {DESCRIPTIONS[item]}{'s' if number > 1 else ''}**"
            )

    def save(self):
        with open(self.fn, "w") as fh:
            fh.write("\n".join(self.out))


@click.command()
@click.argument("group_name", type=str)
@click.option("-r", "reg_nums", type=int, multiple=True)
@click.option("-d", "debug", type=bool)
def build_pdf_from_reg_entries(group_name, reg_nums, debug):
    out_dir = os.getcwd()
    registrations = [
        reg_entry_file_utils.get_model_from_reg_entry(reg_num) for reg_num in reg_nums
    ]
    renderer = RegistrationRenderer(registrations, group_name, ".")
    pdf_fn = build_pdf.build_pdf(out_dir, renderer.fn, pull=False, debug=debug)
    if debug:
        print(pdf_fn)
    return Path(pdf_fn)


if __name__ == "__main__":
    build_pdf_from_reg_entries()
