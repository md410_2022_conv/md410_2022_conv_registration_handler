import json
import os

from bson import json_util
import click
from dotenv import load_dotenv
from pymongo import MongoClient
import pyperclip
from rich import print

import reg_entry_file_utils

load_dotenv()

client = MongoClient(
    host=os.getenv("MONGODB_HOST"),
    port=int(os.getenv("MONGODB_PORT")),
    username=os.getenv("MONGODB_USERNAME"),
    password=os.getenv("MONGODB_PASSWORD"),
)
db = client["md410_2022_conv"]
collection = db["reg_form"]


def get_next_reg_num():
    try:
        return int([d for d in collection.find().sort("reg_num")][-1]["reg_num"]) + 1
    except Exception:
        return 1


def insert_reg_form(reg_form_dict):
    collection.insert_one(reg_form_dict)


@click.group(context_settings=dict(help_option_names=["-h", "--help"]))
def cli():
    pass


def __download_reg_entry(reg_num):
    q = collection.find_one({"reg_num": reg_num})
    if q:
        with open(f"reg_entry_{reg_num:03}.json", "w") as fh:
            fh.write(json_util.dumps(q))


@cli.command()
def all_emails():
    emails = set(
        [
            e
            for e in [m["attendees"][0].get("email", None) for m in collection.find()]
            if e
        ]
    )
    pyperclip.copy(";".join(emails))
    print(f"{len(emails)} emails copied to the clipboard")


@cli.command()
@click.argument("reg_num", type=int)
def download_reg_entry(reg_num):
    __download_reg_entry(reg_num)


@cli.command()
@click.argument("reg_num", type=int)
def upload_reg_entry(reg_num):
    with open(f"reg_entry_{reg_num:03}.json", "r") as fh:
        d = json_util.loads(fh.read())
    collection.replace_one({"reg_num": reg_num}, d)


@cli.command()
@click.argument("reg_num", type=int)
def insert_reg_entry(reg_num):
    with open(f"reg_entry_{reg_num:03}.json", "r") as fh:
        d = json_util.loads(fh.read())
    collection.insert(d)


@cli.command()
@click.argument("reg_num", type=int)
def add_transport(reg_num):
    reg_entry_file_utils.add_transport(reg_num)


@cli.command()
@click.argument("reg_num", type=int)
@click.argument("confirm", type=bool)
def delete_reg_entry(reg_num, confirm):
    if confirm:
        __download_reg_entry(reg_num)
        q = collection.delete_one({"reg_num": reg_num})
    else:
        print("Delete not confirmed, no action taken")


if __name__ == "__main__":
    cli()
