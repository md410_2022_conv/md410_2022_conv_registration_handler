from collections import namedtuple
import copy
import json
import os
import sys

from md410_2022_conv_common import models, sqs
from rich import print

import create_reg_record
import emailer
import mongo


REGISTRATION_RECORD = namedtuple(
    "RegistrationRecord", ("registration_model", "pdf_path")
)


def split_reg_form_json(reg_form_data):
    first = json.loads(reg_form_data)
    if len(first["attendees"]) <= 1:
        return [first]
    second = copy.deepcopy(first)
    first["attendees"] = [first["attendees"][0]]
    first["items"]["reg"] -= 1
    first["cost"] = 0
    second["attendees"] = [second["attendees"][1]]
    second["items"]["reg"] = 1
    second["items"]["pins"] = 0
    second["items"]["windbreakers"] = []
    second["cost"] = 0
    return [first, second]


def process_reg_form_submissions(reg_form_data_list, test=False, debug=False):
    reg_num = mongo.get_next_reg_num()
    for reg_form_data in reg_form_data_list:
        reg_records = []
        try:
            for reg_form_dict in split_reg_form_json(reg_form_data):
                reg_form_dict["reg_num"] = reg_num
                if not test:
                    mongo.insert_reg_form(reg_form_dict)
                registration = models.Registration(**reg_form_dict)
                pdf_path = create_reg_record.build_pdf_from_model(
                    registration, debug=debug
                )
                reg_num += 1
                reg_records.append(REGISTRATION_RECORD(registration, pdf_path))
            emailer.send_reg_email(reg_records)
        except Exception as e:
            print(e)
            pass


def main(test_file=None, debug=False):
    if test_file:
        debug = True
        with open(test_file, "r") as fh:
            reg_form_data_list = [fh.read()]
    else:
        reg_form_data_list = sqs.read_reg_form_data(max_number_of_messages=10)
    process_reg_form_submissions(
        reg_form_data_list,
        test=test_file is not None,
        debug=debug,
    )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        "MDC 2022 Registration Handler, processing entries from an SQS queue"
    )
    parser.add_argument("-v", action="store_true", help="Emit debug")
    parser.add_argument(
        "--test-file",
        default=None,
        help="Use provided test data rather than the SQS queue",
    )
    parser.add_argument(
        "--schedule-period",
        type=int,
        default=None,
        help="A period of time in seconds to wait before processing the queue again. If not given, the tool runs once and exits",
    )

    args = parser.parse_args()
    main(args.test_file, args.v)
    if args.schedule_period is not None:
        import schedule
        import time

        schedule.every(args.schedule_period).seconds.do(
            main, test_file=args.test_file, debug=args.v
        )
        while True:
            schedule.run_pending()
            time.sleep(1)
