""" Helper functions to work with local reg_entry files, downloaded from mongo
"""

import json

from md410_2022_conv_common import models
from rich import print


def get_model_from_reg_entry(reg_num):
    with open(f"reg_entry_{reg_num:03}.json", "r") as fh:
        d = json.loads(fh.read())
    registration = models.Registration(**d)
    return registration


def add_transport(reg_num):
    with open(f"reg_entry_{reg_num:03}.json", "r") as fh:
        d = json.loads(fh.read())
    d["items"]["transport"] = 1
    d["cost"] = 0
    with open(f"reg_entry_{reg_num:03}.json", "w") as fh:
        json.dump(d, fh)
