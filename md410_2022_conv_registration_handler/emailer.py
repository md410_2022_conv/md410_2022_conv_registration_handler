from collections import namedtuple

import click
from md410_2022_conv_common import emailer
from rich import print

import reg_entry_file_utils

REGISTRATION_RECORD = namedtuple(
    "RegistrationRecord", ("registration_model", "pdf_path")
)


def send_reg_email(registration_records):
    with open(
        f"registration_email_{'multiple' if len(registration_records) > 1 else 'single'}.txt",
        "r",
    ) as fh:
        template = fh.read()

    emails = []
    for registration_record in registration_records:
        emails.extend(registration_record.registration_model.emails)
    emails = list(set(emails))
    print(emails)
    for registration_record in registration_records:
        reg = registration_record.registration_model
        if any(emails):
            emailer.send_mail(
                emails,
                f"Confirmation of registration {reg.reg_num_string} for the Lions 2022 MD410 Convention for {reg.names}",
                template.format(registration=reg),
                [registration_record.pdf_path],
            )
        else:
            # no email addresses available
            emailer.send_mail(
                emailer.BCCS,
                f"2022 MDC - no email addresses for {reg.reg_num_string}",
                "Submitted details attached.",
                [registration_record.pdf_path],
            )


@click.command()
@click.argument("reg_num", type=int)
@click.argument("pdf_path")
def email_reg_record(reg_num, pdf_path):
    registration = reg_entry_file_utils.get_model_from_reg_entry(reg_num)
    send_reg_email([REGISTRATION_RECORD(registration, pdf_path)])


if __name__ == "__main__":
    email_reg_record()
